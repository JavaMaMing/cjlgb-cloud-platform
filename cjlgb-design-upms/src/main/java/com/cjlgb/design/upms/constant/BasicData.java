package com.cjlgb.design.upms.constant;

/**
 * @author WFT
 * @date 2020/7/14
 * description:基础数据
 */
public interface BasicData {

    /**
     * 拥有所有权限的角色
     */
    long SUPER_ROLE_ID = 10001L;


}
