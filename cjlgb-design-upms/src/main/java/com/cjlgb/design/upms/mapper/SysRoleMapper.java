package com.cjlgb.design.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cjlgb.design.common.upms.entity.SysRole;

/**
 * @author WFT
 * @date 2020/7/13
 * description:
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {



}
